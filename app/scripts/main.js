$(document).ready(function() {

  $('.collapse').on('shown.bs.collapse', function() {
    $(this).parent().find('.acc-plus').removeClass('acc-plus').addClass('acc-minus');
  }).on('hidden.bs.collapse', function() {
    $(this).parent().find('.acc-minus').removeClass('acc-minus').addClass('acc-plus');
  });

});